{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ConstrainedClassMethods #-}

-- | Simplicial complexes of arcs and curves, and functions for cutting surfaces.
module Surfacer.ArcComplex
    ( -- * Cutting surfaces
      Cutting
    , cut
    , prop_Dimension
      -- * Arc complexes
    , BoundaryConfiguration(Same, Different)
    , NonseparatingArcs(NonseparatingArcs)
    , NonseparatingNonorientableComplementArcs
        ( NonseparatingNonorientableComplementArcs
        )
    , NonseparatingNonorientableComplementOneSidedClosedArcs
        ( NonseparatingNonorientableComplementOneSidedClosedArcs
        )
      -- * Curve complexes
    , NonseparatingCurves(NonseparatingCurves)
    , NonseparatingNonorientableComplementCurves
        ( NonseparatingNonorientableComplementCurves
        )
    )
where

import           Surfacer.Complex
import           Surfacer.Surface

import           Data.List
import           Test.QuickCheck

-- | A complex of 1-dimensional submanifolds of a surface that can be cut
--   along.
--
-- By "cutting," we mean taking the complement.
class Cutting a where
    -- | Possibilities of surfaces arising from cutting along a \(p\)-simplex.
    --
    -- @[]@ ought to be returned precisely if there are no \(p\)-simplices.
    cut :: a
        -- ^ Complex \(X\); encoding a fortiori a 'Surface' \(S\)
        -> Int
        -- ^ Dimension \(p \geq 0\)
        -> [Surface]
        -- ^ Possible diffeomorphism types of \(S - \sigma\) for
        --   \(\sigma \in X_p\)

    -- | Tests that 'cut' is compatible with 'dim'.
    --
    -- It is expected that 'cut'-ing along a simplex of dimension @dim cpx@
    -- does not produce @[]@, as there should indeed be a simplex of dimension
    -- @dim cpx@. Moreover, 'cut'-ing along a simplex of dimension @dim cpx +
    -- 1@ should produce @[]@, as there ought to be no simplices of dimension
    -- greater than @dim cpx@.
    prop_Dimension :: Complex a => a -> Property
    prop_Dimension cpx =
        dim cpx >= 1 ==>
                    null (cut cpx (dim cpx + 1))
            && not (null (cut cpx (dim cpx)))

-- | A boundary configuration.
--
-- Consider the category of surfaces \(S\) endowed with two distinct,
-- distinguished points $\(b_0, b_1\)$ in the boundary \(\partial S\). For each
-- diffeomorphism type of surfaces, there are two isomorphism classes of
-- objects in this category: one for each boundary configuration.
data BoundaryConfiguration = Same
                          -- ^ \(b_0, b_1\) are in the same boundary component
                           | Different
                          -- ^ \(b_0, b_1\) are in different boundary components
    deriving Eq

-- | The number of boundary components intersecting \(\{b_0, b_1\}\).
--
-- This is useful in formulae. The notation \(r'\) for this number appears to
-- be standard in the literature on Harer stability.
r' :: BoundaryConfiguration -> Int
r' Same      = 1
r' Different = 2

instance Show BoundaryConfiguration where
    show Same      = "i=1"
    show Different = "i=2"
instance Arbitrary BoundaryConfiguration where
    arbitrary = elements [Same, Different]

-- | The complex \(BX^i(S, b_0, b_1)\) of nonseparating systems of arcs on
--   a surface \(S\).
--
-- This complex was first considered by Harer for \(S\) orientable. We
-- implement the case of general \(S\), where it was first treated by
-- <https://arxiv.org/abs/math/0601310 Wahl>.
data NonseparatingArcs = NonseparatingArcs' BoundaryConfiguration Surface
    deriving Eq
pattern NonseparatingArcs
    :: BoundaryConfiguration
    -- ^ Boundary configuration \(i\) for \(b_0, b_1\)
    -> Surface
    -- ^ Surface \(S\) (throws exception if boundary configuration is
    --   impossible on \(S\))
    -> NonseparatingArcs
    -- ^ Complex \(BX^i(S, b_0, b_1)\)
pattern NonseparatingArcs i surface <- NonseparatingArcs' i surface where
    NonseparatingArcs i surface
        | r surface == 0 || i == Different && r surface < 2
        = error "not enough boundary components for configuration to be sensible"
        | otherwise
        = NonseparatingArcs' i surface

instance Show NonseparatingArcs where
    show (NonseparatingArcs i surface) =
        "BX^{" ++ show i ++ "}(" ++ show surface ++ ", b_0, b_1)"
instance Arbitrary NonseparatingArcs where
    arbitrary = do
        surface <- arbitrary `suchThat` ((>0) . r)
        i <- if r surface == 1 then return Same else arbitrary
        return $ NonseparatingArcs i surface

instance Complex NonseparatingArcs where
    -- These formulae are proven by induction on \((g, r')\) for the family of
    -- complexes \(BX(S, \Delta_0, \Delta_1)\) where $r'$ is the number of
    -- boundary components of \(S\) intersecting \(\\Delta_1 \cup \Delta_2\).
    -- Each induction step uses the formula \(\dim(X) = \max_{x_0 \in X_0}(1 +
    -- \dim(\Lk(x_0)))\).
    dim (NonseparatingArcs i (S g _)) = 2 * g - 2 + r' i
    dim (NonseparatingArcs i (N g _)) =     g - 2 + r' i

instance Connective NonseparatingArcs where
    -- Wahl, Theorem 3.2(2)
    conn (NonseparatingArcs i (S g _)) = Only (2 * g + r' i - 3)
    conn (NonseparatingArcs i (N g _)) = Only (    g + r' i - 3)

-- | Possibilities that can arise from cutting a single two-sided arc from a
--   boundary component to itself.
--
-- By "two-sided," we mean that the arc can be lifted to an arc in the
-- orientable double cover, which goes from a boundary component to itself. In
-- intuitive terms, that is to say that it has two "sides," as opposed to
-- having just one side.
--
-- The ambiguity in possible cuts arises from the fact that on even genus
-- nonorientable surfaces, a two-sided arc can have both orientable or
-- nonorientable complement.
cut1TwoSided :: NonseparatingArcs -> [Surface]
cut1TwoSided (NonseparatingArcs Same (S g r))
    -- No arcs from a boundary component to itself on a genus 0 surface can be
    -- nonseparating.
    | g == 0 = []
    | g > 0  = [S (g - 1) (r + 1)]
cut1TwoSided (NonseparatingArcs Same (N g r)) =
    -- two-sided with nonorientable complement
       [ N (g - 2) (r + 1) | g >= 2 ]
    -- two-sided with orientable complement
    ++ [ S (g `div` 2 - 1) (r + 1) | even g && g >= 2 ]

instance Cutting NonseparatingArcs where
    -- Cutting a single arc from a boundary component to itself on an
    -- orientable surface.
    cut cpx@(NonseparatingArcs Same (S _ _)) 0
        = cut1TwoSided cpx
    -- Cutting a single arc from a boundary component to itself on a
    -- nonorientable surface. Note that @g == 0@ cannot occur by invariants of
    -- 'Surface'.
    cut cpx@(NonseparatingArcs Same (N g r)) 0 =
        -- There are 4 types of arcs to consider:
        -- one-sided with nonorientable complement
           [N (g - 1) r]
        -- one-sided with orientable complement
        ++ [ S (g `div` 2) r | odd g ]
        -- two-sided arcs
        ++ cut1TwoSided cpx
    -- Cutting a single arc from a boundary component to another.
    cut (NonseparatingArcs Different surface) 0 =
        -- Such a cut has no effect on genus. It merely decreases the number of
        -- boundary components, as is apparant from local picture considerations.
        [addBoundaries (-1) surface]
    -- Cutting a number of arcs from a boundary component to another.
    cut (NonseparatingArcs Different surface) p =
        -- By first cutting along one arc, the remaining arc will constitute a
        -- system of arcs on the cut surface with opposite boundary
        -- configuration, which we can then cut along.
        cut (NonseparatingArcs Same (addBoundaries (-1) surface)) (p - 1)
    -- Cutting a number of arcs from a boundary component to itself.
    cut a@(NonseparatingArcs Same _) p = nub $ sort $ -- Remove duplicates.
        -- As before, we first cut along one arc and then the remaining arcs.
        -- Cutting along the first arc can either keep or alter the boundary
        -- configuration for the next one. We then recurse.
        concatMap (\cpx -> cut cpx (p - 1))
            -- First cut keeps boundary configuration:
            $  map (NonseparatingArcs Same) (cut a 0)
            -- First cut changes boundary configuration. This can only
            -- happen if we cut along two-sided arcs:
            ++ map (NonseparatingArcs Different) (cut1TwoSided a)

-- | Tests that cutting along an arc increments the Euler characteristic.
_prop_EulerCharacteristicCutArc cpx@(NonseparatingArcs _ surface) p =
    all ((== chi surface + p + 1) . chi) $ cut cpx p

-- | The complex \(\mathcal G^i(S, b_0, b_1)\) of nonseparating systems with
--   nonorientable complement of arcs on a surface \(S\).
--
-- This complex was first treated by <https://arxiv.org/abs/math/0601310 Wahl>.
--
-- If \(S\) is orientable, the complex is defined to be empty.
data NonseparatingNonorientableComplementArcs
    = NonseparatingNonorientableComplementArcs' BoundaryConfiguration Surface
    deriving Eq
pattern NonseparatingNonorientableComplementArcs ::
    BoundaryConfiguration
    -- ^ Boundary configuration \(i\) for \(b_0, b_1\)
    -> Surface
    -- ^ Surface \(S\) (throws exception if boundary configuration is
    --   impossible on \(S\))
    -> NonseparatingNonorientableComplementArcs
    -- ^ Complex \(\mathcal G^i(S, b_0, b_1)\)
pattern NonseparatingNonorientableComplementArcs i surface
    <- NonseparatingNonorientableComplementArcs' i surface where
    NonseparatingNonorientableComplementArcs i surface
        | r surface == 0 || i == Different && r surface < 2 = error
            "not enough boundary components for configuration to be sensible"
        | otherwise = NonseparatingNonorientableComplementArcs' i surface

instance Show NonseparatingNonorientableComplementArcs where
    show (NonseparatingNonorientableComplementArcs i surface) =
        "\\mathcal G^{" ++ show i ++ "}(" ++ show surface ++ ", b_0, b_1)"
instance Arbitrary NonseparatingNonorientableComplementArcs where
    arbitrary = do
        surface <- arbitrary `suchThat` ((>0) . r)
        i <- if r surface == 1 then return Same else arbitrary
        return $ NonseparatingNonorientableComplementArcs i surface

instance Complex NonseparatingNonorientableComplementArcs where
    -- This formulae can be proven using the induction argument specified over
    -- 'dim' for 'NonseparatingArcs'.
    dim (NonseparatingNonorientableComplementArcs _ (S _ _)) = -1
    dim (NonseparatingNonorientableComplementArcs i (N g _)) = g - 3 + r' i

instance Connective NonseparatingNonorientableComplementArcs where
    conn (NonseparatingNonorientableComplementArcs _ (S _ _)) = Only (-2)
    -- Wahl, Theorem 3.3(2)
    conn (NonseparatingNonorientableComplementArcs i (N g _)) =
        Only (g + r' i - 4)

instance Cutting NonseparatingNonorientableComplementArcs where
    -- Simply filter the 'cut's from 'NonseparatingArcs' to only retain the
    -- nonorientable ones. This is correct by definition.
    cut (NonseparatingNonorientableComplementArcs i surface) p = filter
        isNonorientable
        allCuts
      where allCuts = cut (NonseparatingArcs i surface) p

-- | The complex \(\mathcal G(S, \vec b_0)\) of nonseparating systems with
--   nonorientable complement of one-sided arcs from \(b_0\) to itself on \(S\).
--
-- This complex was first treated by <https://arxiv.org/abs/math/0601310 Wahl>.
-- If \(S\) is orientable, the complex is defined to be empty.
newtype NonseparatingNonorientableComplementOneSidedClosedArcs
    = NonseparatingNonorientableComplementOneSidedClosedArcs' Surface
    deriving Eq
pattern NonseparatingNonorientableComplementOneSidedClosedArcs ::
    Surface
    -- ^ Surface \(S\) (throws exception if boundary is empty)
    -> NonseparatingNonorientableComplementOneSidedClosedArcs
    -- ^ Complex \(\mathcal G(S, \vec b_0)\)
pattern NonseparatingNonorientableComplementOneSidedClosedArcs surface
    <- NonseparatingNonorientableComplementOneSidedClosedArcs' surface where
    NonseparatingNonorientableComplementOneSidedClosedArcs surface
        | r surface == 0 = error
            "not enough boundary components for configuration to be sensible"
        | otherwise = NonseparatingNonorientableComplementOneSidedClosedArcs' surface

instance Show NonseparatingNonorientableComplementOneSidedClosedArcs where
    show (NonseparatingNonorientableComplementOneSidedClosedArcs surface) =
        "\\mathcal G(" ++ show surface ++ ", \\vec{b_0})"
instance Arbitrary NonseparatingNonorientableComplementOneSidedClosedArcs where
    arbitrary = do
        surface <- arbitrary `suchThat` ((>0) . r)
        return $ NonseparatingNonorientableComplementOneSidedClosedArcs surface

instance Complex NonseparatingNonorientableComplementOneSidedClosedArcs where
    dim (NonseparatingNonorientableComplementOneSidedClosedArcs (S _ _)) = -1
    -- This formula is a consequence of Wahl, Proposition 4.1(c).
    dim (NonseparatingNonorientableComplementOneSidedClosedArcs (N g _)) = g - 2

instance Connective NonseparatingNonorientableComplementOneSidedClosedArcs where
    conn (NonseparatingNonorientableComplementOneSidedClosedArcs (S _ _)) =
        Only (-2)
    -- Wahl, Theorem 3.3(1)
    conn (NonseparatingNonorientableComplementOneSidedClosedArcs (N g _)) =
        Only (g + 1 - 4)

instance Cutting NonseparatingNonorientableComplementOneSidedClosedArcs where
    -- Wahl, Proposition 4.1(c)
    cut (NonseparatingNonorientableComplementOneSidedClosedArcs (N g r)) p =
        [ N (g - k) (r + k - p - 1)
        | k <- [p + 1 .. 2 * p + 1]
        , (g - k) > 0
        ]
    cut (NonseparatingNonorientableComplementOneSidedClosedArcs (S _ _)) _ = []

-- | Tests that cutting along an arc increments the Euler characteristic.
_prop_EulerCharacteristicCutOneSidedArc
    cpx@(NonseparatingNonorientableComplementOneSidedClosedArcs surface) p =
    all ((== chi surface + p + 1) . chi) $ cut cpx p

-- | The complex \(\mathcal C_0(S)\) of nonseparating systems of curves on
--   a surface \(S\).
--
-- This complex was first considered by Harer.
newtype NonseparatingCurves = NonseparatingCurves Surface deriving Eq

instance Show NonseparatingCurves where
    show (NonseparatingCurves surface) = "C_0(" ++ show surface ++ ")"
instance Arbitrary NonseparatingCurves where
    arbitrary = NonseparatingCurves <$> arbitrary

instance Complex NonseparatingCurves where
    dim (NonseparatingCurves (S g _)) = g - 1
    dim (NonseparatingCurves (N g _)) = g - 1

instance Connective NonseparatingCurves where
    -- Wahl, Theorem 5.4
    conn (NonseparatingCurves (S g _)) = Only (g - 2)
    conn (NonseparatingCurves (N g _)) = Only ((g - 3) `div` 2)

instance Cutting NonseparatingCurves where
    -- Cutting a single circle on an orientable surface.
    cut (NonseparatingCurves (S g r)) 0
        | g == 0 = []
        | g > 0  = [S (g - 1) (r + 2)]
    -- Cutting a single circle on a nonorientable surface. Note that @g == 0@
    -- cannot occur by invariants of 'Surface'.
    cut (NonseparatingCurves (N g r)) 0 =
        -- There are 4 types of circles to consider:
        -- one-sided with nonorientable complement
           [N (g - 1) (r + 1)]
        -- one-sided with orientable complement
        ++ [ S (g `div` 2) (r + 1) | odd g ]
        -- two-sided with nonorientable complement
        ++ [ N (g - 2) (r + 2) | g >= 2 ]
        -- two-sided with orientable complement
        ++ [ S (g `div` 2 - 1) (r + 2) | even g && g >= 2 ]
    -- Cutting a multiple circles on a nonorientable surface.
    cut a@(NonseparatingCurves _) p = nub $ sort $ -- Remove duplicates.
        -- Cut along a single circle and then the rest, recursing.
        concatMap (\s -> cut (NonseparatingCurves s) (p - 1))
                  (cut a 0)

-- | Tests that cutting along a circle does not change the Euler characteristic.
_prop_EulerCharacteristicCutCurve cpx@(NonseparatingCurves surface) p =
    all ((== chi surface) . chi) $ cut cpx p

-- | The complex \(\mathcal D(S)\) of nonseparating systems with nonorientable
--   complement of curves on a surface \(S\).
--
-- This complex was treated by <https://arxiv.org/abs/math/0601310 Wahl>.
--
-- If \(S\) is nonorientable, it is empty by definition.
newtype NonseparatingNonorientableComplementCurves
    = NonseparatingNonorientableComplementCurves Surface
    deriving Eq

instance Show NonseparatingNonorientableComplementCurves where
    show (NonseparatingNonorientableComplementCurves surface) =
        "\\mathcal D(" ++ show surface ++ ")"
instance Arbitrary NonseparatingNonorientableComplementCurves where
    arbitrary = NonseparatingNonorientableComplementCurves <$> arbitrary

instance Complex NonseparatingNonorientableComplementCurves where
    dim (NonseparatingNonorientableComplementCurves (S _ _)) = -1
    dim (NonseparatingNonorientableComplementCurves (N g _)) = g - 2

instance Connective NonseparatingNonorientableComplementCurves where
    conn (NonseparatingNonorientableComplementCurves (S _ _)) = Only (-2)
    -- Wahl, Theorem 5.5
    conn (NonseparatingNonorientableComplementCurves (N g _)) =
        Only ((g - 5) `div` 2)

instance Cutting NonseparatingNonorientableComplementCurves where
    -- Simply filter the 'cut's from 'NonseparatingCurves' to only retain the
    -- nonorientable ones. This is correct by definition.
    cut (NonseparatingNonorientableComplementCurves surface) p = filter
        isNonorientable
        allCuts
      where allCuts = cut (NonseparatingCurves surface) p
