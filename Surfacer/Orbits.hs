{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

-- | Semisimplicial sets of orbits of the mapping class group action.
module Surfacer.Orbits
    ( -- * Semisimplicial sets
      SemiSimplicialSet
    , face
    , simplices
    , simplexDim
    , prop_SimplicialIdentities
    , differential
      -- * Orbits under mapping class groups
    , ModMCG
      -- ** Constructing orbit semisimplicial sets (syntax hack)
    , MCG(MCG)
    , MCGOrbits
    , (//)
    )
where

import           Surfacer.Surface
import           Surfacer.Pairing               ( Pairing )
import qualified Surfacer.Pairing              as Pairing
import           Surfacer.Permutation           ( LabeledPermutation )
import qualified Surfacer.Permutation          as Permutation
import           Surfacer.ArcComplex
import           Surfacer.OrientedSystem

import           Data.Maybe
import           Data.List
import           Test.QuickCheck

-- | Produces a list of integers which is supported in a single item and is
--   zero elsewhere.
concentrated
    :: Int
    -- ^ Length @n@ of desired list
    -> Int
    -- ^ Index @i@ for supported item (throws exception if out of bounds)
    -> Int
    -- ^ Content @x@ of supported item
    -> [Int]
    -- ^ List of length @n@ which is zero everywhere except at @i@ where it is
    --   @x@
concentrated n i x
    | i < n     = replicate i 0 ++ [x] ++ replicate (n - i - 1) 0
    | otherwise = error "index out of bound"

-- | A semisimplicial set 'ss' with simplices of type 'simplex'.
--
-- The set of simplices \(X_p\) is encoded as a list '[simplex]'. Therefore, in
-- addition to the structure of a semisimplicial set, 'SemiSimplicialSet' also
-- provides a fixed way to index simplices by nonnegative integers.
class SemiSimplicialSet ss simplex | ss -> simplex where
    -- | The dimension of a simplex.
    simplexDim
        :: ss
        -- ^ Semisimplicial set \(X_*\)
        -> simplex
        -- ^ Simplex \(\sigma \in X_p\)
        -> Int
        -- ^ Dimension \(p\)

    -- | The face of a simplex.
    --
    -- This function ought to satisfy the simplicial identity:
    --
    -- prop> face i c (face j c σ) == face (j - 1) c (face i c σ)
    face
        :: Int
        -- ^ Subscript \(0 \leq i \leq p\)
        -> ss
        -- ^ Semisimplicial set \(X_*\)
        -> simplex
        -- ^ Simplex \(\sigma \in X_p\)
        -> simplex
        -- ^ Face \(d_i \sigma \in X_{p - 1}\)

    -- | The simplices in a given dimension.
    simplices
        :: Int
        -- ^ Dimension \(p\)
        -> ss
        -- ^ Semisimplicial set \(X_*\)
        -> [simplex]
        -- ^ Simplices \(X_p\) in any order

    -- | Tests the simplicial identities.
    --
    -- It is not assumed that the 'Arbitrary' instance for 'simplex' actually
    -- generates simplices in the list produced by 'simplices'. If such a
    -- 'simplex' is not produced, the test is discarded. Beware that in some cases,
    -- this will cause discarding of too many tests, and therefore be a source
    -- of inefficiency.
    prop_SimplicialIdentities
        :: Eq simplex
        => ss
        -> simplex
        -> Property
    prop_SimplicialIdentities c σ = p >= 2 && σ `elem` simplices p c ==>
        do
            i <- chooseInt (0, p - 1)
            j <- chooseInt (i + 1, p)
            return $ printTestCase
                ("(i,j) = " ++ show (i, j))
                (face i c (face j c σ) == face (j - 1) c (face i c σ))
      where p = simplexDim c σ

    -- | A variant of 'face' where instead of taking a 'simplex' of dimension
    --   @p@ it takes an index in @simplices p@.
    enumeratedFace
        :: Eq simplex
        => Int
        -- ^ Subscript \(0 \leq i \leq p\)
        -> ss
        -- ^ Semisimplicial set \(X_*\) / @x@
        -> Int
        -- ^ Dimension \(p\)
        -> Int
        -- ^ Index for \(\sigma \in X_p\) in @simplices p x@
        -> Int
        -- ^ Face \(d_i \sigma in X_{p - 1}\) represented by an index in
        --   @simplices (p - 1) x@
    enumeratedFace i c p s = fromJust
        $ elemIndex (face i c σ) xpMinus1
      where
        σ        = xp !! s
        xp       = simplices p       c -- \(X_p\)
        xpMinus1 = simplices (p - 1) c -- \(X_{p - 1}\)
    -- | The matrix for a differential in the simplicial chain complex arising
    --   from the semisimplicial set 'ss'.
    --
    -- The inner lists represent the columns of the matrix; that is, the
    -- items of the outer list correspond to simplices in the source of the
    -- differential. This matrix convention aligns with that of SageMath.
    --
    -- Columns and rows are ordered according to the order of the simplices
    -- produced by 'simplices'.
    --
    -- This will result in an infinite loop if 'simplices' produces an infinite
    -- list. In principle, one could implement the function in a lazy way so as
    -- to avoid this behavior. However, this would not be of much use, as there
    -- is no way to compute the homology of infinite differentials in a finite
    -- amount of time.
    differential
        :: Eq simplex
        => Int
        -- ^ Dimension \(i\)
        -> ss
        -- ^ Semisimplicial set \(X_*\)
        -> [[Int]]
        -- ^ Matrix for \(\partial_i : C_i(X_*) \to C_{i - 1}(X_*)\)
    differential 0 c = map (const []) (simplices 0 c)
    differential p c =
        [ col column | (column, _) <- zip [0..] xp ]
      where
        xp         = simplices p       c -- \(X_p\)
        xpMinus1   = simplices (p - 1) c -- \(X_{p - 1}\)
        -- Calculates the column at @column@.
        col column =
            -- Compute the alternating sum of faces.
            let summands = [ concentrated (length xpMinus1)
                                          (enumeratedFace i c p column)
                                          ((-1) ^ i)
                           | i <- [0 .. p]
                           ]
            in  foldl' (zipWith (+)) (replicate (length xpMinus1) 0) summands

-- | A complex 'cpx' modulo the action of the mapping class group.
newtype ModMCG cpx = ModMCG cpx

instance Arbitrary cpx => Arbitrary (ModMCG cpx) where
    arbitrary = ModMCG <$> arbitrary

instance Show (ModMCG NonseparatingArcs) where
    show (ModMCG cpx@(NonseparatingArcs _ surface)) =
        show cpx ++ " / " ++ "\\Gamma(" ++ show surface ++ ")"
-- | Orbits are classified by permutations.
instance SemiSimplicialSet (ModMCG NonseparatingArcs)
                           (LabeledPermutation Orientation) where
    simplexDim _ σ = length (Permutation.domain σ) - 1
    face i _ σ = Permutation.face i σ
    -- TODO: We only implement this for orientable surfaces, due to how
    --       'OrientedSystem' is limited right now.
    simplices p (ModMCG (NonseparatingArcs i (S g r))) =
        [ σ
        | σ <- Permutation.allLabeledPermutations (p + 1) [Clockwise]
        -- Stipulate that σ should be realizable as a system of arcs.
        , isJust $ cutSystem $ twoBasepoints (S g r) i σ
        ]

instance Show (ModMCG NonseparatingNonorientableComplementOneSidedClosedArcs) where
    show (ModMCG cpx@(NonseparatingNonorientableComplementOneSidedClosedArcs surface)) =
        show cpx ++ " / " ++ "\\Gamma(" ++ show surface ++ ")"
-- | Orbits are classified by pairings.
instance SemiSimplicialSet (ModMCG NonseparatingNonorientableComplementOneSidedClosedArcs)
                           Pairing where
    simplexDim _ pairing = Pairing.numPairs pairing - 1
    face i _ pairing = Pairing.face i pairing
    simplices _ (ModMCG (NonseparatingNonorientableComplementOneSidedClosedArcs (S _ _))) = []
    simplices p (ModMCG (NonseparatingNonorientableComplementOneSidedClosedArcs (N g r))) =
        [ pairing
        | pairing <- Pairing.allPairings (p + 1)
        -- Stipulate that the pairing should be realizable as a system of
        -- closed, one-sided arcs with nonorientable complement.
        , isJust $ cutSystem $ oneBasepoint (N g r) pairing
        ]

-- | Tests that above a certain range all pairings represent orbits.
_prop_AllPairings c@(ModMCG (NonseparatingNonorientableComplementOneSidedClosedArcs surface)) =
    g surface < 10
    && isNonorientable surface
    ==> simplices p c == Pairing.allPairings (p + 1)
  where p = g surface `div` 2 - 1

instance Show (ModMCG NonseparatingNonorientableComplementArcs) where
    show (ModMCG cpx@(NonseparatingNonorientableComplementArcs _ surface)) =
        show cpx ++ " / " ++ "\\Gamma(" ++ show surface ++ ")"

-- | Orbits are classified by labeled permutations.
instance SemiSimplicialSet (ModMCG NonseparatingNonorientableComplementArcs)
                           (LabeledPermutation Orientation) where
    simplexDim _ σ = length (Permutation.domain σ) - 1
    face i _ σ = Permutation.face i σ
    simplices _ (ModMCG (NonseparatingNonorientableComplementArcs _ (S _ _))) = []
    simplices p (ModMCG (NonseparatingNonorientableComplementArcs i (N g r))) =
        [ σ
        | σ <- Permutation.allLabeledPermutations (p + 1) [Clockwise, Anticlockwise]
        -- Stipulate that σ should be realizable as a system of arcs with
        -- nonorientable complement.
        , isJust $ cutSystem $ twoBasepoints (N g r) i σ
        ]

-- | Tests that above a certain range all labeled permutations represent orbits.
_prop_AllPermutations c@(ModMCG ((NonseparatingNonorientableComplementArcs i surface))) =
    g surface < 10
    && isNonorientable surface
    && i == Different
    ==> simplices p c
        == Permutation.allLabeledPermutations (p + 1) [Clockwise, Anticlockwise]
  where p = (g surface - 1) `div` 2

-- | A vacuous type facilitating the syntactic hack @complex // MCG@ for
--   constructing orbit complexes.
data MCG = MCG
-- | A type class facilitating the syntactic hack @complex // MCG@ for
--   constructing orbit complexes.
class MCGOrbits cpx where
    -- | Constructs a complex of orbits under the mapping class group action.
    --
    -- >>> NonseparatingNonorientableComplementOneSidedClosedArcs (N 5 1) // MCG
    -- \mathcal G(N_{5, 1}, \vec{b_0}) / \Gamma(N_{5, 1})
    (//) :: cpx -> MCG -> ModMCG cpx
instance MCGOrbits NonseparatingArcs where
    cpx // MCG = ModMCG cpx
instance MCGOrbits NonseparatingNonorientableComplementOneSidedClosedArcs where
    cpx // MCG = ModMCG cpx
instance MCGOrbits NonseparatingNonorientableComplementArcs where
    cpx // MCG = ModMCG cpx
