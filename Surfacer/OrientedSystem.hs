{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TupleSections #-}

-- | Oriented systems of arcs.
module Surfacer.OrientedSystem
    ( Orientation(Clockwise, Anticlockwise)
    , BoundaryCircle(BoundaryCircle)
    , OrientedSystem(surface, basepoints, arcs)
    , mkOrientedSystem
    , cutSystem
    , oneBasepoint
    , twoBasepoints
    )
where

import           Surfacer.Surface
import           Surfacer.Pairing               ( Pairing(Pairing) )
import           Surfacer.ArcComplex
import qualified Surfacer.Pairing              as Pairing
import           Surfacer.Permutation           ( LabeledPermutation )
import qualified Surfacer.Permutation          as Permutation

import           Data.List
import           Data.Maybe
import           Control.Arrow
import           Control.Monad
import           Test.QuickCheck

-- | Applies a lambda to a single item of a list.
--
-- >>> adjust (+1) 0 [23, 11]
-- [24, 11]
adjust :: (a -> a) -> Int -> [a] -> [a]
adjust f 0 (x : xs) = f x : xs
adjust f n (x : xs) =   x : adjust f (n - 1) xs
-- | Replaces an item of a list with a new value and discards the old value.
--
-- >>> update 7 1 [23, 11]
-- [23, 7]
update :: a -> Int -> [a] -> [a]
update x = adjust (const x)
-- | Deletes an item from a list.
--
-- >>> [23, 11] `deleteAt` 1
-- [23]
deleteAt :: [a] -> Int -> [a]
deleteAt ls i | i < length ls = take i ls ++ drop (1 + i) ls
              | otherwise     = error "index out of bound"
-- | Combines two items in a list using a function.
--
-- The contents of the items are passed to the lambda in the order in which the
-- indices were specified.
--
-- If the two indices are equal, an exception is thrown, as opposed to passing
-- the same item twice.
--
-- >>> squash (++) 1 0 ["a", "b", "c"]
-- ["ba", "c"]
squash :: (a -> a -> a) -> Int -> Int -> [a] -> [a]
squash f n m ls | n < m     = adjust (`f` mContent) n mRemoved
                | n > m     = squash (flip f) m n ls
                | otherwise = error "you cannot squash an item with itself"
  where
    mContent = ls !! m
    mRemoved = ls `deleteAt` m

-- | The orientation of a boundary component specified relative to the
--   clockwise orientation of the boundary.
--
-- We assume that our surfaces are always endowed with a fixed, ambient
-- "clockwise" orientation of the boundary, to which the 'Orientation' is
-- relative. If the surface is orientable, we stipulate that this ambient
-- orientation extends to an orientation of the surface.
data Orientation = Clockwise | Anticlockwise deriving (Eq, Show)
-- | Reverses the orientation.
reverseOrientation :: Orientation -> Orientation
reverseOrientation Clockwise     = Anticlockwise
reverseOrientation Anticlockwise = Clockwise

instance Arbitrary Orientation where
    arbitrary = elements [Clockwise, Anticlockwise]

-- | A boundary component equipped with oriented basepoints.
--
-- Each basepoint is labeled by a 'label'. Paired with orientations, these
-- are stored in a list. The order of the items in the list represents the
-- order of basepoints met by transversing the boundary component along the
-- fixed clockwise orientation. Thus, if a basepoint has orientation
-- 'Clockwise', its orientation points towards the next basepoint in the list.
--
-- We only care about the list up to a rotation; that is, it is not important
-- what item the list starts on, only which items it contains and what order
-- these are in.
--
-- It is expected that each label is unique, although the constructor does not
-- enforce this. However, the constructor for 'OrientedSystem' enforces this
-- property.
newtype BoundaryCircle label = BoundaryCircle [(label, Orientation)]

-- | A 'BoundaryCircle' is represented by a sequence of labeled arrows,
--   pointing in the direction of the orientation.
instance Show label => Show (BoundaryCircle label) where
    -- TODO: Get rid of this.
    show (BoundaryCircle []) = "no basepoints"
    show (BoundaryCircle [(l, Clockwise)]) =
        "-" ++ show l ++ "->"
    show (BoundaryCircle [(l, Anticlockwise)]) =
        "<-" ++ show l ++ "-"
    show (BoundaryCircle (b0 : rest)) =
        show b0 ++ " " ++ show (BoundaryCircle rest)

-- | The index for a basepoint given its label.
--
-- This throws an exception if no basepoint is assigned the label.
findLabel :: Eq label => label -> BoundaryCircle label -> Int
findLabel b0 (BoundaryCircle c) = fromJust $ findIndex ((== b0) . fst) c
-- | The orientation for a basepoint specified by its label.
--
-- This throws an exception if no basepoint is assigned the label.
orientation :: Eq label => BoundaryCircle label -> label -> Orientation
orientation (BoundaryCircle c) b0 = fromJust $ lookup b0 c
-- | Does the boundary circle contain a basepoint with this label?
contains :: Eq label => label -> BoundaryCircle label -> Bool
contains b0 (BoundaryCircle ls) = any ((== b0) . fst) ls

-- | Rotates a 'BoundaryCircle' to place a specified basepoint in the beginning
--   of the list.
--
-- >>> BoundaryCircle [(0, Clockwise), (2, Anticlockwise)] `rotateTo` 2
-- <-2- -0->
rotateTo :: Eq label => BoundaryCircle label -> label -> BoundaryCircle label
rotateTo (BoundaryCircle c) b0 = BoundaryCircle
    $ take (length c) (drop n (cycle c))
  where n = findLabel b0 (BoundaryCircle c)

-- | Reverses the order of basepoints as well as the individual orientations at
--   each basepoint.
--
-- Morally, what this does is to reverse the ambient orientation of the boundary
-- component. In particular, it doesn't change the "internal structure" of the
-- 'BoundaryCircle'.
--
-- >>> reverseDirection $ BoundaryCircle [(0, Clockwise), (1, Anticlockwise)]
-- -1-> <-0-
reverseDirection :: BoundaryCircle label -> BoundaryCircle label
reverseDirection (BoundaryCircle ls) = BoundaryCircle
    $ reverse $ map (second reverseOrientation) ls
-- | Arranges that a given basepoint has a given 'Orientation' by applying
--   'reverseDirection' if necessary.
--
-- >>> arrangeOrientation Anticlockwise 0 $ BoundaryCircle [(0, Clockwise), (1, Anticlockwise)]
-- -1-> <-0-
-- >>> arrangeOrientation Clockwise 0 $ BoundaryCircle [(0, Clockwise), (1, Anticlockwise)]
-- -0-> <-1
arrangeOrientation
    :: Eq label
    => Orientation
    -> label
    -> BoundaryCircle label
    -> BoundaryCircle label
arrangeOrientation o b0 c | orientation c b0 == o = c
                          | otherwise             = reverseDirection c

-- | Connects two boundary components by cutting along an arc between two
--   basepoints and dropping these basepoints, resulting in a boundary circle
--   compatible with the initial orientations.
--
-- This only makes use of the fixed orientation of the boundary and not the
-- prescribed orientations at the basepoints. That is, it simply rotates and
-- concatenates.
--
-- For example,
--
-- >  /-0---1-\          /-5---6-\
-- >  |       |          |       |
-- >  5       2=========10       7
-- >  |       |          |       |
-- >  \-4---3-/          \-9---8-/
--
-- becomes
--
-- >  /-0---1--------------5---6-\
-- >  |                          |
-- >  5                          7
-- >  |                          |
-- >  \-4---3--------------9---8-/,
--
-- where each point keeps the same orientation.
connectCompatible
    :: Eq label
    => (BoundaryCircle label, label)
    -- ^ First boundary circle \(C_1\) and basepoint \(b_0 \in C\)
    -> (BoundaryCircle label, label)
    -- ^ Second boundary circle \(C_2\) and basepoint \(b_1 \in C_2\)
    -> BoundaryCircle label
    -- ^ Boundary circle \(C'\) arising from connecting \(C_1, C_2\) along
    --   \(b_0, b_1\)
connectCompatible (c0, b0) (c1, b1) =
    -- Drop @b0@ and @b1@ from the list.
    BoundaryCircle $ tail c0Rotated ++ tail c1Rotated
  where
    BoundaryCircle c0Rotated = c0 `rotateTo` b0
    BoundaryCircle c1Rotated = c1 `rotateTo` b1
-- | Connects two boundary components by cutting along an oriented (see
--   'OrientedSystem') arc on a *nonorientable* surface between two oriented
--   basepoints and dropping these basepoints.
connectOrientedNonorientable
    :: Eq label
    => (BoundaryCircle label, label)
    -- ^ First boundary circle \(C_1\) and basepoint \(b_0 \in C\)
    -> (BoundaryCircle label, label)
    -- ^ Second boundary circle \(C_2\) and basepoint \(b_1 \in C_2\)
    -> BoundaryCircle label
    -- ^ Boundary circle \(C'\) arising from connecting \(C_1, C_2\) along an
    --   oriented arc between \(b_0, b_1\)
connectOrientedNonorientable (c0, b0) (c1, b1) =
    -- Consider the cut
    --
    -- >  /-------\          /-------\
    -- >  |       |          |       |
    -- >  |      ↓b0========b1↑      |
    -- >  |       |          |       |
    -- >  \-------/          \-------/.
    --
    -- Transversing the new boundary clockwise, we go around the first original
    -- boundary component along the orientation of @b0@, starting at @b0@, and
    -- then around the second original boundary component, in the direction of
    -- @b1$, starting at @b1$.
    connectCompatible
        -- By assumption, the surfaces in question are nonorientable, and we
        -- are thus allowed to just switch the orientation of the entire
        -- boundary component so as to arrange the situation in the diagram
        -- above. Indeed, this corresponds to applying a (non-boundary-fixing)
        -- diffeomorphism reversing the orientation of said boundary
        -- components, and surely such a diffeomorphism will not have any
        -- effect on the diffeomorphism type of the complement of the arcs.
        (c0, b0)
        -- If @b0@ pointed clockwise, then the new @b1@ shall point
        -- counterclockwise, and so on.
        (arrangeOrientation (reverseOrientation (orientation c0 b0)) b1 c1, b1)

-- | Cuts a boundary component along a two-sided arc into two boundary
--   components.
--
-- By "two-sided," we do not mean relative to the prescribed orientation of
-- basepoints, but rather in the absolute sense of the surface, that is, that
-- the clockwise orientation transfers along the arc to the anticlockwise
-- orientation (the arc has two "sides"). In particular, the orientations of
-- the basepoints will not affect the result.
--
-- As before, the basepoints for the arc that is cut along are dropped.
--
-- For example,
--
-- >  /---6---\
-- >  |   ←  ↑0- - - -\
-- >  |       |       |
-- >  |      ↑1
-- >  5↓      |       |arc
-- >  |      ↑2
-- >  |       |       |
-- >  |   ←  ↑3- - - -/
-- >  \---4---/
--
-- becomes
--
-- >      /-------\         /---6---\
-- >      1↑      |         |   ←   |
-- >  (   |       |    ,    5↓      |   ),
-- >      2↑      |         |   ←   |
-- >      \-------/         \---4---/
--
-- in spite of @0@ and @1@ being oriented to indicate a one-sided arc.
cutTwoSided
    :: Eq label
    => BoundaryCircle label
    -- ^ Boundary circle \(C\)
    -> label
    -- ^ First basepoint \(b_0\)
    -> label
    -- ^ Second basepoint \(b_1\)
    -> (BoundaryCircle label, BoundaryCircle label)
    -- ^ Resulting circles \(C_1, C_2\) from cutting \(C\) along two-sided arc
    --   between \(b_0, b_1\)
cutTwoSided c b0 b1 =
    -- Split basepoints into those between @b0@ and @b1@ and the rest.
    ((BoundaryCircle . tail) *** (BoundaryCircle . tail)) $ splitAt b1At ls
  where
    c'                = c `rotateTo` b0
    BoundaryCircle ls = c'
    b1At              = findLabel b1 c'

-- | Cuts a boundary component along a one-sided arc.
--
-- In particular, the orientations of the basepoints will not affect the
-- result.
--
-- As before, the basepoints for the arc that is cut along are dropped.
--
-- For example,
--
-- >  /---6---\
-- >  |   ←  ↓0- - - -\
-- >  |       |       |
-- >  |      ↑1
-- >  5↓      |       |arc
-- >  |      ↑2
-- >  |       |       |
-- >  |   ←  ↑3- - - -/
-- >  \---4---/
--
-- becomes
--
-- >  /---6---\
-- >  |   ←   |
-- >  |      ↓2
-- >  5↓      |
-- >  |      ↓1
-- >  |   ←   |
-- >  \---4---/
cutOneSided
    :: Eq label
    => BoundaryCircle label
    -- ^ Boundary circle \(C\)
    -> label
    -- ^ First basepoint \(b_0\)
    -> label
    -- ^ Second basepoint \(b_1\)
    -> BoundaryCircle label
    -- ^ Resulting circle \(C'\) from cutting \(C\) along one-sided arc between
    --   \(b_0, b_1\)
cutOneSided c b0 b1 =
    -- As in the example above, the effect of cutting along a one-sided arc is
    -- not to create any new boundary components, but to take a segment of the
    -- boundary and reverse the direction of that segment (i.e., reverse the
    -- ordering and reversing the orientation to account for that reversal). To
    -- see this, by the classification of surfaces, it suffices to consider the
    -- case in which you cut along an arc that passes through a single
    -- crosscap,
    --
    -- >   j
    -- >  /-------\a
    -- >  |       +-------\
    -- >  |       |e     f|b
    -- >  |       |       ⊗
    -- >  |       |d     c|g
    -- >  |       +-------/
    -- >  \-------/h
    -- >   i
    --
    -- Following the boundary around from @a@ to @j@, one finds (1) there
    -- are no new boundary components after cutting, and (2) the @e@ to @d@
    -- segment is opposite to the fixed clockwise orientation and is the only
    -- such segment, thus warrenting the aforementioned alterations.
    BoundaryCircle
        $  reverse (map (second reverseOrientation) (tail part1))
        ++ tail part2
  where
    c'                = c `rotateTo` b0
    BoundaryCircle ls = c'
    b1At              = findLabel b1 c'
    -- Split the boundary circle into two parts, just as you would do in the
    -- case of a two-sided arc.
    (part1, part2)    = splitAt b1At ls

-- | A potential orbit of a nonseparating system of isotopy classes of oriented
--   arcs, with nonorientable complement if the surface is nonorientable.
--
-- Each basepoint holds an orientation. By "oriented arc," we mean an arc that
-- admits a tubular neighborhood, which can be oriented in a way that restricts
-- to the prescribed orientation at the endpoints.
-- <https://arxiv.org/abs/math/0601310 Wahl> calls this property "one-sided,"
-- not to be confused with the other notion of "one-sided arcs."
--
-- By "orbit," we refer to being an orbit under the action by the mapping class
-- group on the set of such systems.
--
-- By "potential," we mean that the system might not actually be realizable as
-- a system of arcs on the surface, due to genus constraints. In other words,
-- any system can be uniquely described by an 'OrientedSystem', but the converse
-- is not true. An 'OrientedSystem' is realizable as a genuine system of arcs
-- if and only if 'cutSystem' does not return 'Nothing', and in this case,
-- 'OrientedSystem' uniquely determines the (orbit of the) system.
--
-- The reason that we are stipulating nonorientable complement only when the
-- underlying surface is nonorientable is that we wish to handle the case of
-- orientable surfaces as well, and we can do this with just one implementation
-- instead of two, although making the sacrifice of perfect intuitiveness to
-- have this slightly peculiar difference in behavior of the two cases.
data OrientedSystem label =
    OrientedSystem { surface :: Surface
                   -- ^ The surface upon which the system is defined.
                   , basepoints :: [BoundaryCircle label]
                   -- ^ The boundary circles upon which there are basepoints.
                   --   Labels are assumed to be unique. A boundary component
                   --   need not be represented in this list.
                   , arcs :: [(label, label)]
                   -- ^ The arcs in the system. To simplify the implementation,
                   --   we require that each basepoint may participate in at
                   --   most one arc. In this way, we will not have to worry
                   --   about ordering at the germs of arcs near the
                   --   basepoints. Such orderings are instead encoded in the
                   --   configurations of the basepoints.
                   }
-- | Creates a 'OrientedSystem', throwing an exception in the event of
--   malformed data.
mkOrientedSystem
    :: (Eq label, Ord label)
    => Surface
    -- ^ Surface upon which the system is defined
    -> [BoundaryCircle label]
    -- ^ Boundary components with basepoints (throws exception if label is used
    --   twice or if more 'BoundaryCircle's are specified than there are
    --   boundary components)
    -> [(label, label)]
    -- ^ Arcs specified by endpoints (throws exception if (1) a basepoint does
    --   not exist, (2) a basepoint participates in more than one arc, or (3) if
    --   an arc has both of the ends being the same basepoint; see explanation
    --   as to why at 'OrientedSystem')
    -> OrientedSystem label
    -- ^ Resulting system of arcs
mkOrientedSystem surface basepoints arcs
    -- Check that there are enough boundary components.
    | length basepoints > r surface
    = error
        "specifying basepoints on more boundary components than there actually are"
    -- Check that no label is used twice, by concatenating all the lists of
    -- labels and checking that the items are unique in the resulting list.
    | let labels = concatMap (\(BoundaryCircle ls) -> map fst ls) basepoints
      in  sort labels /= nub (sort labels)
    = error "label is used twice"
    -- Check that every basepoint that is used is actually defined.
    | or [    not (any (contains b0) basepoints)
           || not (any (contains b1) basepoints)
         | (b0, b1) <- arcs
         ]
    = error "not all basepoints are defined"
    -- Check that basepoints are used at most once by forming a list of
    -- instances where label is used and checking uniqueness of the items.
    | let supportedBasepoints = uncurry (++) (unzip arcs)
      in  sort supportedBasepoints /= nub (sort supportedBasepoints)
    = error "each basepoint must meet an arc at most once"
    | otherwise
    = OrientedSystem { surface, basepoints, arcs }

instance Show label => Show (OrientedSystem label) where
    show OrientedSystem { surface, basepoints, arcs } =
           "(S = "
        ++ show surface
        ++ ", \\del S = "
        ++ show basepoints
        ++ ", arcs = "
        ++ show arcs
        ++ ")"

-- | Cuts along a system of arcs, if it can be realized.
--
-- This returns 'Nothing' if and only if the 'OrientedSystem' cannot be
-- realized as a system of arcs on the surface. Otherwise, it produces the
-- 'OrientedSystem' that results from cutting along the arcs.
--
-- When an arc is cut, the surface is cut accordingly and the arc as well as
-- its endpoints are removed from the 'OrientedSystem'. In particular, the
-- resulting 'OrientedSystem' has an empty list @[]@ of arcs, but the
-- basepoints that were not a part of any arc are retained.
cutSystem :: Eq label => OrientedSystem label -> Maybe (OrientedSystem label)

-- If no arcs are to be cut, the system stays unchanged.
cutSystem sys@OrientedSystem { arcs = [] } = Just sys

-- Suppose we want to cut just a single arc between \(b_0\) and \(b_1\) on a
-- surface \(S\). First, the case where \(S\) is orientable.
cutSystem OrientedSystem { surface = S g r, basepoints, arcs = [(b0, b1)] }
    -- Remember that we stipulated that the orientations are specified relative
    -- to a global orientation of the surface, when the surface is orientable.
    -- Therefore, if the clockwise orientations shall transfer to the clockwise
    -- orientation, the arc is not realizable.
    | b0Orientation == b1Orientation = Nothing
    -- The case in which \(b_0\) and \(b_1\) lie on distinct boundary
    -- components, but are compatible with some global orientation of the
    -- surface.
    | b0Boundary /= b1Boundary = Just OrientedSystem
        -- The effect on the underlying surface is to just merge two boundary
        -- components. On the other hand, the genus is unchanged, as can be
        -- seen by Euler characteristic considerations.
        { surface    = S g (r - 1)
        -- Combine the participating 'BoundaryCircle's using 'connectCompatible.
        , basepoints = squash
            (\b0Circle b1Circle ->
                 connectCompatible (b0Circle, b0) (b1Circle, b1)
            )
            b0Boundary
            b1Boundary
            basepoints
        -- No arcs remain.
        , arcs       = []
        }
    -- The case in which \(S\) is orientable, \(b_0\) and \(b_1\) lie on the
    -- same boundary component, these are oriented incompatibly, and there is
    -- enough genus so that they are realizable. In particular, the arc is
    -- two-sided.
    | b0Boundary == b1Boundary && b0Orientation /= b1Orientation && g > 0 = Just
        OrientedSystem
            -- The effect on the underlying surface is to trade 1 genus for a
            -- boundary component, by Euler characteristic considerations.
            { surface    = S (g - 1) (r + 1)
            -- The modification to the 'BoundaryCircle' is handled by
            -- 'cutTwoSided', which splits @b0Circle@ into two
            -- 'BoundaryCircle's. See its documentation.
            , basepoints =
                let (newBoundary1, newBoundary2) = cutTwoSided b0Circle b0 b1
                in  newBoundary2 : update newBoundary1 b0Boundary basepoints
            -- No arcs remain.
            , arcs       = []
            }
  where
    -- Indices for boundary components.
    b0Boundary    = fromJust $ findIndex (contains b0) basepoints
    b1Boundary    = fromJust $ findIndex (contains b1) basepoints
    -- Boundary components.
    b0Circle      = basepoints !! b0Boundary
    b1Circle      = basepoints !! b1Boundary
    -- Orientations.
    b0Orientation = orientation b0Circle b0
    b1Orientation = orientation b1Circle b1
-- The case in which \(S\) is nonorientable.
cutSystem OrientedSystem { surface = N g r, basepoints, arcs = [(b0, b1)] }
    -- The case in which \(b_0\) and \(b_1\) lie on distinct boundary components.
    | b0Boundary /= b1Boundary = Just OrientedSystem
        -- The effect on the underlying surface is to just merge two boundary
        -- components. On the other hand, the genus is unchanged, as can be
        -- seen by Euler characteristic considerations.
        { surface    = N g (r - 1)
        -- Combine the participating 'BoundaryCircle's using
        -- 'connectOrientedNonorientable'.
        , basepoints = squash
            (\b0Circle b1Circle ->
                 connectOrientedNonorientable (b0Circle, b0) (b1Circle, b1)
            )
            b0Boundary
            b1Boundary
            basepoints
        -- No arcs remain.
        , arcs       = []
        }
    -- The case in which \(S\) is nonorientable, \(b_0\) and \(b_1\) lie on the
    -- same boundary component and are oriented incompatibly, and there is
    -- enough genus so that they are realizable (with nonorientable
    -- complement). In particular, the arc is two-sided.
    | b0Boundary == b1Boundary && b0Orientation /= b1Orientation && g > 2 = Just
        OrientedSystem
            -- This is effectively the same as the orientable case above.
            { surface    = N (g - 2) (r + 1)
            , basepoints =
                let (newBoundary1, newBoundary2) = cutTwoSided b0Circle b0 b1
                in  newBoundary2 : update newBoundary1 b0Boundary basepoints
            , arcs       = []
            }
    -- The case in which \(S\) is nonorientable, \(b_0\) and \(b_1\) lie on the
    -- same boundary component and are oriented compatibly, and there is enough
    -- genus so that they are realizable (with nonorientable complement). In
    -- particular, the arc is one-sided.
    | b0Boundary == b1Boundary && b0Orientation == b1Orientation && g > 1 = Just
        OrientedSystem
            -- The effect on the underlying surface is nil on the number of
            -- boundary components. However, the genus is decremented, as can
            -- be shown by Euler characteristic considerations.
            { surface    = N (g - 1) r
            -- The modification to the 'BoundaryCircle' is handled by
            -- 'cutOneSided'. See its documentation.
            , basepoints = update (cutOneSided b0Circle b0 b1)
                                  b0Boundary
                                  basepoints
            -- No arcs remain.
            , arcs       = []
            }
  where
    -- Indices for boundary components.
    b0Boundary    = fromJust $ findIndex (contains b0) basepoints
    b1Boundary    = fromJust $ findIndex (contains b1) basepoints
    -- Boundary components.
    b0Circle      = basepoints !! b0Boundary
    b1Circle      = basepoints !! b1Boundary
    -- Orientations.
    b0Orientation = orientation b0Circle b0
    b1Orientation = orientation b1Circle b1
-- The remaining unhandled cases of one arc are those in which there is not
-- enough genus for the arc to be realizable. For example, there is no
-- two-sided arc with nonorientable complement on \(N_{2, 1}\). See definition
-- of 'OrientedSystem'.
cutSystem OrientedSystem { arcs = [_] } = Nothing

-- The case in which there are multiple arcs.
cutSystem OrientedSystem { surface, basepoints, arcs = arc : rest } = do
    -- The problem of cutting \(n\) arcs can be broken down to cutting \(1\)
    -- arc \(n\) times, or in recursive terms, cutting \(1\) arc then cutting
    -- \(n - 1\) arcs. If the first cut produces 'Nothing', the entire
    -- collection is not realizable either.
    OrientedSystem { surface = surface', basepoints = basepoints' } <-
        cutSystem OrientedSystem { surface, basepoints, arcs = [arc] }
    cutSystem OrientedSystem { surface    = surface'
                             , basepoints = basepoints'
                             , arcs       = rest
                             }

-- | The oriented system of one-sided arcs arising from having all basepoints
--   on a single boundary component and arcs corresponding to a pairing of these.
--
-- The name of the function refers not to the number of basepoints in the
-- 'OrientedSystem' (which will usually be more than one), but rather to the
-- fact that without the stipulation of basepoints not being reusable, we could
-- represent such system with a single basepoint, with the pairing then
-- specifying the ordering of the germs of arcs near the basepoint.
oneBasepoint
    :: Surface
    -- ^ Surface on which the arcs live (throws exception if there are no
    --   boundary components)
    -> Pairing
    -- ^ Pairing specifying the arcs
    -> OrientedSystem Int
    -- ^ Resulting system of arcs
oneBasepoint surface p@(Pairing ls)
    | r surface == 0 = error "not enough boundary components"
    | otherwise = OrientedSystem
        { surface
        , basepoints = [BoundaryCircle [ (i, Clockwise) | i <- Pairing.members p ]]
        , arcs       = ls
        }

-- | The system arising from having half the basepoints on one boundary
--   component and the other on another (respectively on the other side of the
--   same boundary component) with arcs specified by a (labeled) permutation.
--
-- The 'Orientation' label describes the orientation at the first end, whereas
-- the last end will always be oriented 'Anticlockwise'. For example, in the
-- case of configuration 'Same', 'Clockwise' describes a two-sided arc and
-- 'Anticlockwise' a one-sided arc.
--
-- The name of the function refers not to the number of basepoints in the
-- 'OrientedSystem' (which will usually be more than two), but rather to the
-- fact that without the stipulation of basepoints not being reusable, we could
-- represent such system with a two basepoint, with the permutation then
-- specifying the ordering of arcs.
twoBasepoints
    :: Surface
    -- ^ Surface on which the arcs live
    -> BoundaryConfiguration
    -- ^ Boundary configuration (throws exception if there are not enough
    --   boundary components)
    -> LabeledPermutation Orientation
    -- ^ Permutation of the basepoints (ordered according to the
    --   clockwise orientation) labeled by what orientation should slide to
    --   'Anticlockwise' along the corresponding arc
    -> OrientedSystem Int
    -- ^ Resulting system of arcs
twoBasepoints surface config σ
    | r surface == 0 || config == Different && r surface < 2
    = error "not enough boundary components for configuration to be sensible"
    | otherwise = OrientedSystem
        { surface
        , basepoints =
            -- To create two basepoints for each element in the domain of the
            -- permutation, we use even labels for the first ends and odd
            -- labels on the second ones.
            case config of
                Different ->
                    [ BoundaryCircle
                        [ (evenify i, σ `Permutation.labelAt` i)
                        | i <- Permutation.domain σ
                        ]
                    , BoundaryCircle
                        [ (oddify i, Anticlockwise)
                        | i <- Permutation.domain σ
                        ]
                    ]
                Same ->
                    [ BoundaryCircle
                       $  [ (evenify i, σ `Permutation.labelAt` i)
                          | i <- Permutation.domain σ
                          ]
                       ++ [ (oddify i, Anticlockwise)
                          | i <- Permutation.domain σ
                          ]
                    ]
        , arcs =
            [ (evenify i, oddify (Permutation.permute σ i))
            | i <- Permutation.domain σ
            ]
        }
  where
    evenify n = 2 * n
    oddify  n = 2 * n + 1

-- | Combines 'OrientedSystem's for \(S_1\) and \(S_2\) to the one for
--   \(S_1 \# S_2\).
connectedSum :: OrientedSystem Int -> OrientedSystem Int -> OrientedSystem Int
connectedSum OrientedSystem { surface = surface1
                           , basepoints = basepoints1
                           , arcs = arcs1
                           }
            OrientedSystem { surface = surface2
                           , basepoints = basepoints2
                           , arcs = arcs2
                           }
    = OrientedSystem { surface = surface1 # surface2
                     , basepoints = basepoints1' ++ basepoints2'
                     , arcs = arcs1' ++ arcs2'
                     }
  where
    -- As before, we avoid collision of labels by using even for one part of
    -- the basepoints and odd for the rest.
    basepoints1' = map
        (\(BoundaryCircle ls) ->
            BoundaryCircle $ map (first (2 *)) ls
        )
        basepoints1
    basepoints2' = map
        (\(BoundaryCircle ls) ->
            BoundaryCircle $ map (first (\n -> 2 * n + 1)) ls
        )
        basepoints2
    arcs1' = map (\(x, y) -> (2 * x, 2 * y))         arcs1
    arcs2' = map (\(x, y) -> (2 * x + 1, 2 * y + 1)) arcs2

-- TODO: Improve generation of random systems. For example, right now there are
--       no triangles of arcs. Consider making a recursive generator.
-- | Generates a basic 'OrientedSystem' in which the basepoints are
--   concentrated on just one or two boundary components.
simpleArbitrary :: Gen (OrientedSystem Int)
simpleArbitrary = do
    surface <- arbitrary `suchThat` ((>= 1) . r)
    i <- arbitrary
    σ <- arbitrary
    p <- arbitrary
    if i == Different && r surface >= 2 then
        -- Use random permutation. We do not generate configuration 'Same', as
        -- all of these will be covered as a special case of the pairing case.
        return $ twoBasepoints surface Different σ
    else
        -- Use random pairing.
        return $ oneBasepoint surface p
instance Arbitrary (OrientedSystem Int) where
    arbitrary = do
        -- Use 'simpleArbitrary' a random number of times and combine the
        -- results using 'connectedSum'.
        n <- chooseInt (0, 5)
        base <- arbitrary `suchThat` ((>= 1) . r)
        simples <- replicateM n simpleArbitrary
        foldM ((return .) . connectedSum)
              (mkOrientedSystem base [] [])
              simples
    shrink OrientedSystem { surface, basepoints, arcs } =
        -- The only thing we shrink is the list of arcs.
        [ mkOrientedSystem surface basepoints subarcs
        | subarcs <- subsequences arcs ]

-- | Tests that the cuts arising with one basepoint is among the possibilities
--   generated in "ArcComplex".
_prop_CutWithinRange1 surface p = r surface >= 1 && isNonorientable surface ==>
    case cutSystem (oneBasepoint surface p) of
        Just OrientedSystem { surface = surface' } ->
            surface' `elem` cut
                (NonseparatingNonorientableComplementOneSidedClosedArcs surface)
                (Pairing.numPairs p - 1)
        Nothing -> discard
-- | Tests that the cuts of a nonorientable surface arising with two basepoints
--   on distinct boundary components is among the possibilities generated in
--   "ArcComplex".
_prop_CutWithinRange2
    :: NonseparatingNonorientableComplementArcs
    -> LabeledPermutation Orientation
    -> Property
_prop_CutWithinRange2 cpx@(NonseparatingNonorientableComplementArcs i surface) σ =
    isNonorientable surface ==>
        case cutSystem (twoBasepoints surface i σ) of
            Just OrientedSystem { surface = surface' } ->
                surface' `elem` cut cpx (length (Permutation.domain σ) - 1)
            Nothing -> discard
-- | Tests that the cuts of a orientable surface arising with two basepoints
--   on distinct boundary components is among the possibilities generated in
--   "ArcComplex".
_prop_CutWithinRange3
    :: NonseparatingArcs
    -> LabeledPermutation Orientation
    -> Property
_prop_CutWithinRange3 cpx@(NonseparatingArcs i surface) σ =
    isOrientable surface ==>
        case cutSystem (twoBasepoints surface i σ) of
            Just OrientedSystem { surface = surface' } ->
                surface' `elem` cut cpx (length (Permutation.domain σ) - 1)
            Nothing -> discard
-- | Tests that each cut arc increments the Euler characteristic.
_prop_EulerCharacteristicCut :: OrientedSystem Int -> Bool
_prop_EulerCharacteristicCut sys =
    case cutSystem sys of
        Just sys' -> chi (surface sys') == chi (surface sys) + length (arcs sys)
        Nothing   -> discard
-- | Tests that the possible cuts arising with one basepoint agrees with the
--   lists generated in "ArcComplex".
_prop_AllCuts1 = do
    s <- arbitrary `suchThat` ((>= 1) . r)
    p <- chooseInt (1, 5)
    return
        $  (nub . sort)
               (cut (NonseparatingNonorientableComplementOneSidedClosedArcs s) p)
        == (nub . sort)
               (mapMaybe (fmap surface . cutSystem . oneBasepoint s)
                         (Pairing.allPairings (p + 1))
               )
-- | Tests that the possible cuts of a nonorientable surface arising with two
--   basepoints on distinct boundary components agrees with the lists generated
--   in "ArcComplex".
_prop_AllCuts2 cpx@(NonseparatingNonorientableComplementArcs i s) =
    isNonorientable s ==> do
        p <- chooseInt (1, 4)
        return $ (nub . sort) (cut cpx p) == (nub . sort)
            (mapMaybe
                (fmap surface . cutSystem . twoBasepoints s i)
                (Permutation.allLabeledPermutations
                    (p + 1)
                    [Clockwise, Anticlockwise]
                )
            )
-- TODO: Implement 'OrderedArcs' and 'DisorderedArcs' in "ArcComplex" and
--       simplify these tests using those.
-- | Tests that cutting along ordered systems of arcs between two boundary
--   components agrees with known formulae.
_prop_Ordered (NonseparatingArcs i surface) (NonNegative p) =
    isOrientable surface ==>
        case
            cutSystem
            $ twoBasepoints surface i
            $ Permutation.LabeledPermutation
            $ map (, Clockwise) (reverse [0 .. p])
        of
            Just OrientedSystem { surface = surface' } ->
                case i of
                    Different -> g surface' == g surface - p
                              && r surface' == r surface + p - 1
                    Same      -> g surface' == g surface - p - 1
                              && r surface' == r surface + p + 1
            Nothing -> discard
-- | Tests that cutting along disordered systems of arcs between two boundary
--   components agrees with known formulae.
_prop_Disordered (NonseparatingArcs i surface) (NonNegative p) =
    isOrientable surface ==>
        case
            cutSystem
            $ twoBasepoints surface i
            $ Permutation.LabeledPermutation
            $ map (, Clockwise) [0 .. p]
        of
            Just OrientedSystem { surface = surface' } ->
                case i of
                    -- For proofs of these formulae, see my paper with Oscar
                    -- Harr and Nathalie Wahl.
                    Different -> g surface' == g surface - (p + 1) `div` 2
                              && r surface' == r surface - (p + 1) `mod` 2
                    Same      -> g surface' == g surface - p `div` 2 - 1
                              && r surface' == r surface + (p + 1) `mod` 2
            Nothing -> discard
