{-# LANGUAGE PatternSynonyms #-}

-- | Permutations and labeled permutations.
module Surfacer.Permutation
    ( LabeledPermutation(LabeledPermutation)
    , Permutation
    , permute
    , labelAt
    , domain
    , allLabeledPermutations
    , face
    )
where

import           Data.List
import           Control.Arrow
import           Test.QuickCheck

-- | A labeled permutation.
--
-- A labeled permutation is a tuple \((\sigma, f)\), where
-- \(\sigma : \{0, \ldots, n\} \to \{0, \ldots, n\}\) is a bijection and
-- \(f : \{0, \ldots, n\} \to \texttt{label}\) is a function (a labeling).
--
-- We represent such a labeled permutation by the list of pairs
-- \((\sigma(i), f(i))\) for \(i = 0, \ldots, n\).
--
-- We disallow the empty permutation (a matter of convention).
newtype LabeledPermutation label = LabeledPermutation' [(Int, label)]
    deriving (Eq, Show)
-- | An ordinary, unlabeled permutation.
type Permutation = LabeledPermutation ()
pattern LabeledPermutation
    :: [(Int, label)]
    -- ^ List of pairs \((\sigma(i), f(i))\) (throws exception if empty or if
    --   it is not a valid zero-based permutation)
    -> LabeledPermutation label
    -- ^ Labeled permutation \((\sigma, f)\)
pattern LabeledPermutation ls <- LabeledPermutation' ls where
    LabeledPermutation ls
        | null ls                                     = error "empty permutation is illegal"
        | sort (map fst ls) /= zipWith const [0..] ls = error "invalid permutation"
        | otherwise                                   = LabeledPermutation' ls

instance Arbitrary label => Arbitrary (LabeledPermutation label) where
    arbitrary = do
        -- To avoid combinatorial explosions, we use a relatively small domain.
        -- Alternatively, one could write an efficient ad hoc generation of
        -- permutations.
        n <- chooseInt (1, 5)
        LabeledPermutation' σ' <- elements $ allLabeledPermutations n [()]
        let σ = map fst σ'
        labels <- vectorOf n arbitrary
        return $ LabeledPermutation (zip σ labels)
    -- We shrink by taking faces.
    shrink σ | length (domain σ) > 1 = [face i σ | (i, _) <- zip [0..] (domain σ)]
             | otherwise             = []

-- | The domain of a permutation (as a sorted list).
--
-- >>>domain (Permutation [(2, True), (0, False), (1, False)])
-- [0, 1, 2]
domain :: LabeledPermutation label -> [Int]
domain (LabeledPermutation ls) = zipWith const [0..] ls

-- | Applies permutation.
permute
    :: LabeledPermutation label
    -- ^ (Labeled) permutation \((\sigma, f)\)
    -> Int
    -- ^ \(i\) in domain of \(\sigma\)
    -> Int
    -- ^ \(\sigma(i)\)
permute (LabeledPermutation ls) n = fst (ls !! n)

-- | Applies labeling function.
labelAt
    :: LabeledPermutation label
    -- ^ Labeled permutation \((\sigma, f)\)
    -> Int
    -- ^ \(i\) in domain of \(\sigma\)
    -> label
    -- ^ Label \(f(i)\)
labelAt (LabeledPermutation ls) n = snd (ls !! n)

-- | Decrements integer if it is above a threshold.
--
-- This is useful for getting rid of "holes" left by removing an integer:
--
-- >>> map (lower 4) ([1..3] ++ [5..10])
-- [1, 2, 3, 4, 5, 6, 7, 8, 9]
lower
    :: Int
    -- ^ Threshold \(n\)
    -> Int
    -- ^ Integer to be lowered \(m\) (throws exception if \(n = m\))
    -> Int
    -- ^ \(m - 1\) if \(n < m\) and \(m\) otherwise
lower n m | n < m     = m - 1
          | n > m     = m
          | otherwise = error "encountered removed level"
-- | Deletes an index from a list.
deleteAt
    :: Int
    -- ^ Index @i@ (throws exception if out of bound)
    -> [a]
    -- ^ List @ls@
    -> [a]
    -- ^ @ls@ with index @i@ deleted
deleteAt i ls | i < length ls = take i ls ++ drop (1 + i) ls
              | otherwise     = error "index out of bound"

-- | Removes an element and its image from the domain of a permutation.
--
-- The permutations of various sizes assemble into a semisimplicial set with
-- @face i@ as \(d_i\).
face
    :: Int
    -- ^ Index \(i\) (throws exception if out of bound)
    -> LabeledPermutation label
    -- ^ Labeled permutation \((\sigma, f)\) with domain \(\{0, \ldots, n\}\)
    -> LabeledPermutation label
    -- ^ Labeled permutation with permutation
    --   \(\sigma|\{0, \ldots, n\} - \{i, \sigma(i)\} \to \{0, \ldots, n\} - \{i, \sigma(i)\}\)
    --   and labeling \(f|\{0, \ldots, n\} \setminus \{i, \sigma(i)\})\),
    --   reparameterized by the order preserving bijection
    --   \(\{0, \ldots, n\} \setminus \{i, \sigma(i)\} \to \{0, \ldots, n - 2\}\)
face i (LabeledPermutation ls) = LabeledPermutation
    $ map (first (lower x)) (deleteAt i ls)
  where (x, _) = ls !! i
-- | Tests the simplicial identities.
_prop_SimplicialIdentities σ = n >= 3 ==>
    do
        i <- chooseInt (0, n - 2)
        j <- chooseInt (i + 1, n - 1)
        return $ face i (face j σ) == face (j - 1) (face i σ)
  where n = length (domain σ)

-- | Combinations of a certain length.
--
-- This is a version of 'subsequences' where reordering and repetition are
-- allowed.
combinations
    :: Int
    -- ^ Non-negative integer \(n\)
    -> [a]
    -- ^ List representing a set \(X\)
    -> [[a]]
    -- ^ List representing the set \(X^n\)
combinations 0 _ = []
-- Singleton lists.
combinations 1 ls = map (: []) ls
combinations n ls =
    -- First consider all lists of length @n - 1@ and then prepend each
    -- possible element.
    concatMap (\comb -> [ x : comb | x <- ls ])
              (combinations (n - 1) ls)
-- | Tests that the number of combinations agrees with combinatorial formulae.
_prop_PowerOfTwo = do
    n <- chooseInt (1, 5)
    return $ length (combinations n [0 :: Int, 1]) == 2^n

-- | Lists all possible labeled permutation with a certain domain.
allLabeledPermutations
    :: Int
    -- ^ Number of elements in domain \(k\)
    -> [label]
    -- ^ Possible labels \(X\)
    -> [LabeledPermutation label]
    -- ^ All labeled permutation \((\sigma, f)\) with domain
    --   \(\{0, \ldots, k - 1\}\) and labels in \(X\)
allLabeledPermutations n labels =
    [ LabeledPermutation $ zip σ l
    | σ <- permutations [0 .. n - 1]
    , l <- combinations n labels
    ]
-- | Tests that the number of labeled permutations agrees with combinatorial
--   formulae.
_prop_NumberOfLabeledPermutations = do
    n <- chooseInt (1, 5)
    return $ length (allLabeledPermutations n [0 :: Int, 1]) == 2^n * product [1..n]
